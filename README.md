# McftHelp

A Bukkit server plugin for Minecraft that allows you to make your own help menu per user group.

This was made to suit the needs of the [Diamond Mine](http://diamondmine.net) server. This plugin is based off of [MyHelpPages](http://dev.bukkit.org/server-mods/myhelppages/).

**[License](https://diamondmine.net/plugins/license)** -- **[Issues](https://mcftmedia.atlassian.net/issues/?jql=project=HELP)** --  **[Builds](https://mcftmedia.atlassian.net/builds/browse/HELP-HELP)**
-- **[Download](http://diamondmine.net/plugins/download/McftHelp)** -- **[Bukkit Dev](http://dev.bukkit.org/server-mods/mcfthelp/)**
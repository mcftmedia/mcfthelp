package net.diamondmine.mcfthelp.exceptions;

public class HelpPageException extends Exception {
    private static final long serialVersionUID = 8209516823588659165L;

    /**
     * The type of exception
     * 
     * @author Mitch
     * 
     */
    public enum Type {
        PAGE_NOT_EXIST("That page does not exist."), PAGE_ALREADY_EXIST("That page already exists.");

        private String message;

        private Type(final String message) {
            this.message = message;
        }

        public String getMessage() {
            return message;
        }
    }

    private final Type type;

    public HelpPageException(final Type type) {
        super(type.getMessage());
        this.type = type;
    }

    public HelpPageException(final Type type, final String msg) {
        super(msg);
        this.type = type;
    }

    public HelpPageException(final Type type, final Throwable cause) {
        super(type.getMessage(), cause);
        this.type = type;
    }

    public HelpPageException(final Type type, final String msg, final Throwable cause) {
        super(msg, cause);
        this.type = type;
    }

    public Type getType() {
        return type;
    }
}

package net.diamondmine.mcfthelp;

/**
 * This enum contains permission nodes that are used by the command executor.
 * 
 * @author Mitch
 * 
 */
public enum Permission {
    RELOAD("mcfthelp.reload"), LIST("mcfthelp.list"), MAIN("mcfthelp"), ALLPAGES("mcfthelp.allpages");

    private String node;

    private Permission(final String node) {
        this.node = node;
    }

    /*
     * (non-Javadoc)
     * 
     * @see java.lang.Enum#toString()
     */
    @Override
    public String toString() {
        return node;
    }

    /**
     * Input a page name and this method returns the required permission node.
     * 
     * @param name
     *            The page name
     * @return The permission node
     */
    public static String convertPageNameToPermission(final String name) {
        return MAIN + "." + name;
    }
}

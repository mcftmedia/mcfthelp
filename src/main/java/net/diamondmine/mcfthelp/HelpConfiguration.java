package net.diamondmine.mcfthelp;

import java.util.List;
import java.util.Set;

import net.diamondmine.mcfthelp.exceptions.HelpPageException;
import net.diamondmine.mcfthelp.util.YamlFile;

import org.bukkit.ChatColor;
import org.bukkit.configuration.ConfigurationSection;
import org.bukkit.configuration.file.YamlConfiguration;
import org.bukkit.plugin.java.JavaPlugin;

/**
 * Implementation of the <code>IHelpConfiguration</code> interface.
 * 
 * @author Mitch
 * 
 */
public class HelpConfiguration extends YamlFile implements IHelpConfiguration {
    public HelpConfiguration(final JavaPlugin plugin) {
        super(plugin, "config");
        reload();
        // causes the default-pages.yml to be loaded... if necessary
        if (!getConfig().contains("pages")) {
            addDefaults("default-pages");
        }
    }

    private ConfigurationSection pages() {
        YamlConfiguration config = getConfig();
        // get pages section
        ConfigurationSection result = config.getConfigurationSection("pages");
        if (result == null) {
            // create default if it doesnt exist
            result = config.createSection("pages");
            addDefaults("default-pages");
        }
        return result;
    }

    private ConfigurationSection messages() {
        ConfigurationSection result = getConfig().getConfigurationSection("messages");
        if (result == null) {
            // causes the defaults to be loaded
            reload();
        }
        return result;
    }

    @Override
    public String getMessageNoPagePermission() {
        return messages().getString("noPagePermission");
    }

    @Override
    public String getMessagePageNotFound() {
        return messages().getString("pageNotFound");
    }

    @Override
    public String getMessagePageTile() {
        return messages().getString("pageTitle");
    }

    public void setMessageNoPagePermission(final String value) {
        messages().set("noPagePermission", value);
    }

    public void setMessagePageNotFound(final String value) {
        messages().set("pageNotFound", value);
    }

    public void setMessagePageTile(final String value) {
        messages().set("pageTitle", value);
    }

    @Override
    public Set<String> getPageNames() {
        return pages().getKeys(false);
    }

    @Override
    public boolean hasPage(final String name) {
        return pages().contains(name);
    }

    @Override
    public List<String> getPage(final String name) {
        return pages().getStringList(name);
    }

    public void addPage(final String name, final List<String> lines) throws HelpPageException {
        if (!pages().contains(name)) {
            throw new HelpPageException(HelpPageException.Type.PAGE_ALREADY_EXIST, ChatColor.RED + "Page " + ChatColor.WHITE + "\"" + name + "\""
                    + ChatColor.RED + " already not exists.");
        } else {
            pages().set(name, lines);
        }
    }

    public void removePage(final String name) throws HelpPageException {
        if (!pages().contains(name)) {
            throw new HelpPageException(HelpPageException.Type.PAGE_NOT_EXIST, ChatColor.RED + "Page " + ChatColor.WHITE + "\"" + name + "\"" + ChatColor.RED
                    + " does not exist.");
        } else {
            pages().set(name, null);
        }
    }
}
